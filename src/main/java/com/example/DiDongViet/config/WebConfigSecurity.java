package com.example.DiDongViet.config;

import com.example.DiDongViet.security.jwt.JwtAuthEntryPoint;
import com.example.DiDongViet.security.jwt.JwtAuthFilter;
import com.example.DiDongViet.security.oauth2.service.CustomOAuth2AccountService;
import com.example.DiDongViet.security.oauth2.service.HttpCookieOAuth2AuthorizationRequestRepository;
import com.example.DiDongViet.security.oauth2.service.OAuth2AuthenticationFailureHandler;
import com.example.DiDongViet.security.oauth2.service.OAuth2AuthenticationSuccessHandler;
import com.example.DiDongViet.service.CustomAccountDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
public class WebConfigSecurity {


    @Autowired
    private CustomAccountDetailsService userDetailsService;
    @Autowired
    private CustomOAuth2AccountService customOAuth2AccountService;
    @Autowired
    private OAuth2AuthenticationSuccessHandler oauth2AuthenticationSuccessHandler;
    @Autowired
    private OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;

    @Autowired
    private JwtAuthEntryPoint unauthorizedHandler;

    @Bean
    public JwtAuthFilter authenticationJwtTokenFilter() {
        return new JwtAuthFilter();
    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();

        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());

        return authProvider;
    }


    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authConfig) throws Exception {
        return authConfig.getAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return super.encode(rawPassword);
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return super.matches(rawPassword, encodedPassword);
            }
        };
    }


    private RequestMatcher publicMatchers(){
        List<RequestMatcher> matchers = Arrays.asList(
                new AntPathRequestMatcher("/"),
                new AntPathRequestMatcher("/app/**/*.{js,html}"),
                new AntPathRequestMatcher("/content/**"),
                new AntPathRequestMatcher("/error"),
                new AntPathRequestMatcher("/auth/signup"),
                new AntPathRequestMatcher("/oauth2/**"),
                new AntPathRequestMatcher("/v3/**"),
                new AntPathRequestMatcher("/configuration/ui"),
                new AntPathRequestMatcher("/swagger-resources/**"),
                new AntPathRequestMatcher("/configuration/security"),
                new AntPathRequestMatcher("/swagger-ui.html"),
                new AntPathRequestMatcher("/webjars/**"),
                new AntPathRequestMatcher("/favicon.ico"),
                new AntPathRequestMatcher("/**/*.png"),
                new AntPathRequestMatcher("/**/*.gif"),
                new AntPathRequestMatcher("/**/*.svg"),
                new AntPathRequestMatcher("/**/*.jpg"),
                new AntPathRequestMatcher("/**/*.html"),
                new AntPathRequestMatcher("/**/*.css"),
                new AntPathRequestMatcher("/**/*.js")
        );
        return new OrRequestMatcher(matchers);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(csrf -> csrf.disable())
                .exceptionHandling(exception -> exception.authenticationEntryPoint(unauthorizedHandler))
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authorizeHttpRequests(auth ->
                                auth
                                        .requestMatchers(publicMatchers()).permitAll()
                                        .requestMatchers(HttpMethod.OPTIONS,"/**").permitAll()
                                        //API Account
                                        .requestMatchers(HttpMethod.GET,"/api/account/").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/account/changeRole").hasRole("ADMIN")
                                        .requestMatchers(HttpMethod.GET,"/api/account/changeStatus").hasRole("ADMIN")
                                        .requestMatchers(HttpMethod.GET,"/api/account/search").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/account/role").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/api/account/login").anonymous()
                                        .requestMatchers(HttpMethod.POST,"/api/account/register").anonymous()
                                        .requestMatchers(HttpMethod.POST,"/api/account/changePassword").permitAll()
                                        .requestMatchers(HttpMethod.POST,"/api/account/reset").permitAll()
                                        .requestMatchers(HttpMethod.POST,"/api/account/reset/confirm").permitAll()
                                        //API User
                                        .requestMatchers(HttpMethod.GET,"/api/user").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/user/users").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/user/account").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/api/user").permitAll()
                                        //API Cart
                                        .requestMatchers(HttpMethod.GET,"/carts/all-cart-details").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/carts/user").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET, "/carts/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/carts/clear/**").permitAll()
                                        .requestMatchers(HttpMethod.POST,"/carts/remove-cart-detail/**").permitAll()
                                        .requestMatchers(HttpMethod.POST,"/carts/users").permitAll()
                                        .requestMatchers(HttpMethod.PUT,"/carts/**").permitAll()
                                        //API Category
                                        .requestMatchers(HttpMethod.PUT,"/category/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.DELETE,"/category/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/category").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/category/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET, "/category").permitAll()
                                        //API Comment
                                        .requestMatchers(HttpMethod.DELETE, "/api/comments/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/api/comments/createComment").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/api/comments/product/**").permitAll()
                                        //API DiscountCombo
                                        .requestMatchers(HttpMethod.POST, "/api/discount-combo/update").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST, "/api/discount-combo/add").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST, "/api/discount-combo/add-detail").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET, "/api/discount-combo/list").permitAll()
                                        .requestMatchers(HttpMethod.GET, "/api/discount-combo/list-detail").permitAll()
                                        .requestMatchers(HttpMethod.GET, "/api/discount-combo/get").permitAll()
                                        .requestMatchers(HttpMethod.GET, "/api/discount-combo/get2").permitAll()
                                        .requestMatchers(HttpMethod.GET, "/api/discount-combo/get-by-name").permitAll()
                                        .requestMatchers(HttpMethod.GET, "/api/discount-combo/check").permitAll()
                                        //API DiscountPayment
                                        .requestMatchers(HttpMethod.PUT,"/api/discountpayment/update/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/api/discountpayment/add").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.DELETE,"/api/discountpayment/delete/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/discountpayment/**").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/api/discountpayment/search/**").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/api/discountpayment/all").permitAll()
                                        //API DiscountPaymentDetail
                                        .requestMatchers(HttpMethod.POST,"/api/discountpaymentdetail/add").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/discountpaymentdetail/all").permitAll()
                                        //API Manufacturer
                                        .requestMatchers(HttpMethod.PUT,"/manufacturer/update").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/manufacturer/create").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.DELETE,"/manufacturer/delete").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/manufacturer/**").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/manufacturer/getAll").permitAll()
                                        //API Payment
                                        .requestMatchers(HttpMethod.PUT,"/api/payment/update/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/api/payment/add").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.DELETE,"/api/payment/delete/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/payment/**").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/api/payment/all").permitAll()
                                        //API Product Color
                                        .requestMatchers(HttpMethod.PUT,"/api/product_color/edit/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/product_color/get-list/**").permitAll()
                                        //API Product
                                        .requestMatchers(HttpMethod.PUT,"/products/update").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/products/add").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/products/admin/page").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.DELETE,"/products/delete/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/products").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/products/**").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/products/search").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/products/page").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/products/SortingDESC").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/products/SortingASC").permitAll()
                                        //API Rate
                                        .requestMatchers(HttpMethod.POST,"/api/rates/createRate").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/api/rates/product/**").permitAll()
                                        //API Specification Detail
                                        .requestMatchers( HttpMethod.POST, "/details/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers( HttpMethod.PUT, "/details/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/details/**").permitAll()
                                        //API Store
                                        .requestMatchers(HttpMethod.PUT,"/api/stores/update").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/api/stores/add").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/stores/edit/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.DELETE,"/api/stores/delete/**").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/stores/search").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/api/stores/search2").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/api/stores/get-list").permitAll()
                                        //API Voucher
                                        .requestMatchers(HttpMethod.POST,"/api/voucher/update").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/api/voucher/add").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.POST,"/api/voucher/addVoucherDetail").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/voucher/list").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/voucher/get").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/api/voucher/getAllByName").permitAll()
                                        .requestMatchers(HttpMethod.GET,"/api/voucher/check").permitAll()
                                        //API Discount Payment Detail
                                        .requestMatchers(HttpMethod.POST,"/api/discountpaymentdetail/add").hasAnyRole("ADMIN", "MANAGER")
                                        .requestMatchers(HttpMethod.GET,"/api/discountpaymentdetail/all").permitAll()
                                        //API News
//                                .requestMatchers("/api/editor/news/delete").hasRole("ADMIN")
//                                .requestMatchers("/api/editor/news/update").hasRole("ADMIN")
                                        .anyRequest().authenticated()
                )
                .addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class)
                .logout(
                        logout -> logout
                                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                                .permitAll()
                )
                .oauth2Login(oauth2 -> oauth2
                        .authorizationEndpoint(endpoint -> endpoint
                                .baseUri("/oauth2/authorize")
                                .authorizationRequestRepository(cookieAuthorizationRequestRepository())
                        )
                        .redirectionEndpoint(endpoint -> endpoint
                                .baseUri("/oauth2/callback/*")
                        )
                        .userInfoEndpoint(userInfo -> userInfo
                                .userService(customOAuth2AccountService)
                        )
                        .successHandler(oauth2AuthenticationSuccessHandler)
                        .failureHandler(oAuth2AuthenticationFailureHandler)
                );

        http.authenticationProvider(authenticationProvider());

        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);

        return http.build();
    }
    private AuthorizationRequestRepository<OAuth2AuthorizationRequest> cookieAuthorizationRequestRepository() {
        return new HttpCookieOAuth2AuthorizationRequestRepository();
    }
}
