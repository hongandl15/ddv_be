package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forCreate.CSpecificationDetailDTO;
import com.example.DiDongViet.dto.forDetail.SpecificationDetailDTO;
import com.example.DiDongViet.dto.forList.LSpecificationDetailDTO;
import com.example.DiDongViet.dto.forUpdate.USpecificationDetailDTO;
import com.example.DiDongViet.entity.SpecificationDetail;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface SpecificationDetailService {


    @Transactional
    List<CSpecificationDetailDTO> saveSpecificationDetails(Long productId, List<CSpecificationDetailDTO> cSpecificationDetailDTOList);

    @Transactional
    boolean updateSpecificationDetailByProductId(Long productId, List<USpecificationDetailDTO> specificationDetailDTO);

    List<Map<String, Object>>getListDetailById (Long productId);
}
