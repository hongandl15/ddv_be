package com.example.DiDongViet.service;



import com.example.DiDongViet.dto.forList.LProductDTO;
import com.example.DiDongViet.entity.CategoryC;

import java.util.List;
import java.util.Optional;

public interface CategoryCService {

    CategoryC createCategoryC(CategoryC category);

    List<CategoryC> getAllCategoriesC();

    Optional<CategoryC> getCategoryCById(Long categoryId);

    CategoryC updateCategoryC(CategoryC categoryC);

    boolean deleteCategoryCById(Long categoryCId);

    List<LProductDTO> getProductsByCategoryC(CategoryC categoryC);
}

