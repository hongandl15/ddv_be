package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forCreate.DiscountPaymentDetailDTO;
import com.example.DiDongViet.entity.DiscountPaymentDetail;

import java.util.List;

public interface DiscountPaymentDetailService {
    DiscountPaymentDetail createDiscountPaymentDetail(DiscountPaymentDetailDTO discountPaymentDetailDTO);
    List<DiscountPaymentDetail> getAllDiscountPaymentDetail();
    boolean deleteAllPaymentDetail();
/*    RCheckDiscount existByOrderId(CheckDiscount id);*/
}
