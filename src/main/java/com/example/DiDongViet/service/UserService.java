package com.example.DiDongViet.service;


import com.example.DiDongViet.dto.*;
import com.example.DiDongViet.entity.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {
    User create(RegisterModel registerModel);
    User update(MultipartFile multipartFile, Long id, UserDTO.Update updateUser);
    UserDTO.Response getResponseUserByUserId(Long id);
    UserDTO.Response getResponseUserByAccountId(Long id);
    User getUserByUserId(Long id);
    User getUserByAccountId(Long id);
    List<User> getAll();
}
