package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forCreate.CRequestVoucher;
import com.example.DiDongViet.dto.forList.RLResponseVoucher;
import com.example.DiDongViet.entity.Voucher;

import java.util.List;

public interface VoucherService {
    Voucher insert(CRequestVoucher cRequestVoucher);
    Voucher getOne(Long id);
    List<RLResponseVoucher> getALl();
    Voucher update (Voucher voucher);
    List<RLResponseVoucher> search(String s);
    Voucher check(String code);
}
