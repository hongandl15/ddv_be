package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forUpdate.UProductDTO;
import com.example.DiDongViet.dto.forCreate.ProductDTO;
import com.example.DiDongViet.dto.forCreate.ProductResponseDTO;
import com.example.DiDongViet.dto.forDetail.DProductDTO;
import com.example.DiDongViet.dto.forList.LProductDTO;
import com.example.DiDongViet.entity.Product;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface ProductService {
    @Transactional
    ProductResponseDTO addProduct(ProductDTO productDto);


    List<Product> getAllProducts();

    List<LProductDTO> searchProducts(String keyword);

    DProductDTO getProductById(Long productId);

    @Transactional
    UProductDTO updateProductInfo(UProductDTO updatedProductDto);


    @Transactional
    boolean deleteProduct(Long productId);

    Page<LProductDTO> Sorting(int page, Sort.Direction direction);

    Page<Product> getAllProducts(int page);

    Page<LProductDTO> getAllProductDTOs(int page, Sort.Direction direction);


}
