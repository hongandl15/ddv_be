package com.example.DiDongViet.service;

import com.example.DiDongViet.entity.Payment;

import java.util.List;
import java.util.Optional;

public interface PaymentService {
    Payment createPayment(Payment payment);
    List<Payment> getAllPayment();
    Optional<Payment> getPaymentById(Long id);
    Payment updatePayment(Payment payment);
    boolean deletePaymentByTd(Long id);
}
