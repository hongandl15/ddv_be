package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forCreate.CRequestDiscountCombo;
import com.example.DiDongViet.entity.DiscountCombo;

import java.util.List;

public interface DiscountComboService {
    DiscountCombo insert(CRequestDiscountCombo cRequestDiscountCombo);

    DiscountCombo getOne(Long id);

    List<DiscountCombo> getAll();

    DiscountCombo update(DiscountCombo discountCombo);

    List<DiscountCombo> search(String s);

}

