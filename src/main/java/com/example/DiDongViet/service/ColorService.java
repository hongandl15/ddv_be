package com.example.DiDongViet.service;


import com.example.DiDongViet.entity.Color;

import java.util.List;
import java.util.Optional;

public interface ColorService {
    Color createColor(Color productColor);

    List<Color> getAllColor();

    Optional<Color> getColorById(Long colorId);

    Color updateColor(Color color);

    boolean deleteColorById(Long colorId);
}
