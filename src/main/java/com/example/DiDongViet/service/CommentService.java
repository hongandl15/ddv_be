package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forCreate.CommentDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {
    List<CommentDTO> getCommentsByProductId(Long productId);
    CommentDTO createComment(CommentDTO commentDTO);
    void deleteComment(Long id);
}
