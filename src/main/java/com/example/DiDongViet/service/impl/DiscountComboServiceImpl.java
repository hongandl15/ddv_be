package com.example.DiDongViet.service.impl;

import com.example.DiDongViet.dto.forCreate.CRequestDiscountCombo;
import com.example.DiDongViet.entity.DiscountCombo;
import com.example.DiDongViet.repositories.DiscountComboRepository;
import com.example.DiDongViet.service.DiscountComboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountComboServiceImpl implements DiscountComboService {
    @Autowired
    DiscountComboRepository discountComboRepository;
    @Override
    public DiscountCombo insert(CRequestDiscountCombo cRequestDiscountCombo) {
        DiscountCombo discountCombo = new DiscountCombo();
        discountCombo.setDiscountName(cRequestDiscountCombo.getDiscountName());
        discountCombo.setPercent(cRequestDiscountCombo.getPercent());
        discountCombo.setDateStart(cRequestDiscountCombo.getDateStart());
        discountCombo.setDateEnd(cRequestDiscountCombo.getDateEnd());
        return discountComboRepository.save(discountCombo);
    }

    @Override
    public DiscountCombo getOne(Long id) {
        return discountComboRepository.findById(id).get();
    }

    @Override
    public List<DiscountCombo> getAll() {
        List<DiscountCombo> discountComboList = discountComboRepository.findAll();
        return discountComboList;
    }

    @Override
    public DiscountCombo update(DiscountCombo discountCombo) {
        return discountComboRepository.save(discountCombo);
    }

    @Override
    public List<DiscountCombo> search(String s) {
        return discountComboRepository.findAllByDiscountNameContains(s);
    }
}

