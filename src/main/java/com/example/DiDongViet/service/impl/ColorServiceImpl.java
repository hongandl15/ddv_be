package com.example.DiDongViet.service.impl;

import com.example.DiDongViet.entity.Color;
import com.example.DiDongViet.repositories.ColorRepository;
import com.example.DiDongViet.service.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ColorServiceImpl implements ColorService {
    @Autowired private ColorRepository colorRepository;
    @Override
    public Color createColor(Color color) {
        return colorRepository.save(color);
    }

    @Override
    public List<Color> getAllColor() {
        return colorRepository.findAll();
    }

    @Override
    public Optional<Color> getColorById(Long colorId) {
        return colorRepository.findById(colorId);
    }
    @Override
    public Color updateColor(Color color) {
        return colorRepository.save(color);
    }
    @Override
    public boolean deleteColorById(Long colorId) {
        if (colorRepository.existsById(colorId)) {
            colorRepository.deleteById(colorId);
            return true;
        }
        return false;
    }
}
