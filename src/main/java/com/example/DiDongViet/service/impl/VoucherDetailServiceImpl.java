package com.example.DiDongViet.service.impl;

import com.example.DiDongViet.dto.forCreate.CRequestVoucherDetail;
import com.example.DiDongViet.entity.VoucherDetail;
import com.example.DiDongViet.repositories.VoucherDetailRepository;
import com.example.DiDongViet.service.VoucherDetailService;
import com.example.DiDongViet.service.VoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VoucherDetailServiceImpl implements VoucherDetailService {
    @Autowired
    VoucherDetailRepository voucherDetailRepository;
    @Autowired
    VoucherService voucherService;
    @Override
    public VoucherDetail insert(CRequestVoucherDetail cRequestVoucherDetail) {
        VoucherDetail voucherDetail = new VoucherDetail(cRequestVoucherDetail.getOrdersID(), voucherService.check(cRequestVoucherDetail.getVoucherCode()));
        return voucherDetailRepository.save(voucherDetail);
    }
}
