package com.example.DiDongViet.service.impl;

import com.example.DiDongViet.dto.forCreate.CRequestDiscountComboDetail;
import com.example.DiDongViet.entity.CategoryC;
import com.example.DiDongViet.entity.DiscountComboDetail;
import com.example.DiDongViet.repositories.CategoryCRepository;
import com.example.DiDongViet.repositories.DiscountComboDetailRepository;
import com.example.DiDongViet.repositories.DiscountComboRepository;
import com.example.DiDongViet.service.DiscountComboDetailService;
import com.example.DiDongViet.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiscountComboDetailServiceImpl implements DiscountComboDetailService {
    @Autowired
    DiscountComboDetailRepository discountComboDetailRepository;
    @Autowired
    CategoryCRepository categoryCRepository;
    @Autowired
    ProductService productService;
    @Autowired
    DiscountComboRepository discountComboRepository;
    @Override
    public DiscountComboDetail insert(CRequestDiscountComboDetail cRequestDiscountComboDetail) {
        DiscountComboDetail discountComboDetail = new DiscountComboDetail();
        discountComboDetail.setCategoryC1(categoryCRepository.findById(cRequestDiscountComboDetail.getCategoryId1()).get());
        discountComboDetail.setCategoryC2(categoryCRepository.findById(cRequestDiscountComboDetail.getCategoryId2()).get());
        discountComboDetail.setDiscountCombo(discountComboRepository.findById(cRequestDiscountComboDetail.getDiscountComboId()).get());
        return discountComboDetailRepository.save(discountComboDetail);
    }

    @Override
    public List<DiscountComboDetail> getlistByC1(Long idC1) {
        return discountComboDetailRepository.findAllByCategoryC1_IdOrCategoryC2_Id(idC1,idC1);
    }

    //    @Override
//    public List<DiscountComboDetail> findBy2Cate(Long id1, Long id2) {
//        Long cate1 = productService.getProductById(id1).getProduct().getCategoryC().getId();
//        Long cate2 = productService.getProductById(id2).getProduct().getCategoryC().getId();
//        return discountComboDetailRepository.findAllByCategoryC1_IdAndCategoryC2_Id(cate1,cate2);
//    }
    @Override
    public List<DiscountComboDetail> findBy2Cate(Long id1, Long id2) {
        CategoryC cate1 = productService.getProductById(id1).getProduct().getCategoryC();
        CategoryC cate2 = productService.getProductById(id2).getProduct().getCategoryC();
        return discountComboDetailRepository.findAllByCategoryC1AndCategoryC2(cate1,cate2);
    }

    @Override
    public List<DiscountComboDetail> getAll() {
        return discountComboDetailRepository.findAll();
    }
}

