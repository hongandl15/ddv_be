package com.example.DiDongViet.service.impl;

import com.example.DiDongViet.entity.StoreDetail;
import com.example.DiDongViet.repositories.StoreDetailRepository;
import com.example.DiDongViet.service.StoreDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StoreDetailServiceImpl implements StoreDetailService {
    @Autowired
    private StoreDetailRepository storeDetailRepository;

    @Override
    public StoreDetail createStoreDetail(StoreDetail storeDetail) {
        return storeDetailRepository.save(storeDetail);
    }

    @Override
    public List<StoreDetail> getAllStoresDetail() {
        return storeDetailRepository.findAll();
    }

    @Override
    public Optional<StoreDetail> getStoreDetailById(Long storeDetailId) {
        return storeDetailRepository.findById(storeDetailId);
    }

    @Override
    public StoreDetail updateStoreDetail(StoreDetail storeDetail) {
        return storeDetailRepository.save(storeDetail);
    }
    @Override
    public boolean deleteStoreDetailById(Long storeDetailId) {
        if (storeDetailRepository.existsById(storeDetailId)) {
            storeDetailRepository.deleteById(storeDetailId);
            return true;
        }
        return false;
    }
}

