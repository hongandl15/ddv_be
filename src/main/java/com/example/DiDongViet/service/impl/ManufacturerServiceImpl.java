package com.example.DiDongViet.service.impl;

import com.example.DiDongViet.dto.forList.LProductManufacturer;
import com.example.DiDongViet.entity.Manufacturer;
import com.example.DiDongViet.entity.Product;
import com.example.DiDongViet.entity.ProductImage;
import com.example.DiDongViet.repositories.ManufacturerRepository;
import com.example.DiDongViet.repositories.ProductImageRepository;
import com.example.DiDongViet.repositories.ProductRepository;
import com.example.DiDongViet.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductImageRepository productImageRepository;
    private ManufacturerRepository manufacturerRepository;
    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository){
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    @Transactional
    public Manufacturer createManufacturer(Manufacturer manufacturer){
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    @Transactional
    public Manufacturer updateManufacturer(Manufacturer manufacturer){
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    @Transactional
    public Boolean deleteManufacturer(Long id){
        if(manufacturerRepository.existsById(id)){
            manufacturerRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public List<Manufacturer> getAllManufacturer()
    {
        return manufacturerRepository.findAll();
    }

    @Override
    public List<LProductManufacturer> getProductsByManufacturer(Manufacturer manufacturer) {
        List<Product> products = productRepository.findByManufacturer(manufacturer);
        List<LProductManufacturer> lProductDTOs = new ArrayList<>();
        for (Product product : products) {
            LProductManufacturer lProductDTO = new LProductManufacturer();
            lProductDTO.setId(product.getId());
            lProductDTO.setName(product.getName());
            lProductDTO.setPrice(product.getPrice());
            lProductDTO.setRate(product.getRate());
            lProductDTO.setImageLinks(getFirstImageLink(product));
            lProductDTOs.add(lProductDTO);
        }
        return lProductDTOs;
    }
    private String getFirstImageLink(Product product) {
        List<ProductImage> productImages = productImageRepository.findByProductId(product.getId());
        if (!productImages.isEmpty()) {
            return productImages.get(0).getLink();
        }
        return null;
    }

}
