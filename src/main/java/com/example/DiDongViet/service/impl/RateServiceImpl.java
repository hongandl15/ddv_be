package com.example.DiDongViet.service.impl;

import com.example.DiDongViet.dto.forCreate.RateDTO;
import com.example.DiDongViet.dto.forCreate.RateResponseDTO;
import com.example.DiDongViet.dto.forList.LRateDTO;
import com.example.DiDongViet.entity.Product;
import com.example.DiDongViet.entity.Rate;
import com.example.DiDongViet.entity.RateImage;
import com.example.DiDongViet.repositories.ProductRepository;
import com.example.DiDongViet.repositories.RateImageRepository;
import com.example.DiDongViet.repositories.RateRepository;
import com.example.DiDongViet.service.RateService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RateServiceImpl implements RateService {
    @Autowired
    private RateRepository rateRepository;
    @Autowired
    private RateImageRepository rateImageRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private FirebaseImageService firebaseImageService;

    @Override
    public List<LRateDTO> getRateByProductId(Long productId) {
        Optional<Product> productOptional = productRepository.findById(productId);
        if (productOptional.isEmpty()) {
        }

        Product product = productOptional.get();

        List<Rate> rates = rateRepository.findAllByProductId_Id(productId);
        List<LRateDTO> rateDTOS = new ArrayList<>();
        for (Rate rate : rates){
            LRateDTO rateDTO = new LRateDTO();
            rateDTO.setFullName(rate.getFullName());
            rateDTO.setPhoneNumber(rate.getPhoneNumber());
            rateDTO.setEvaluate(rate.getEvaluate());
            rateDTO.setStar(rate.getStar());
            rateDTO.setTime(rate.getTime());
            rateDTO.setProductId(product.getId());

            List<String> imageRate = rateImageRepository.findAllByRateIdAndProductId(rate.getId(), productId)
                    .stream()
                    .map(RateImage::getLink)
                    .collect(Collectors.toList());

            rateDTO.setImageRate(new HashSet<>(imageRate));
            double averageRating = avgByProductId(productId);
            rateDTO.setAverageRating(averageRating);
            rateDTOS.add(rateDTO);
        }
        return rateDTOS;
    }

    public double avgByProductId(Long productId) {
        List<Rate> rates = rateRepository.findAllByProductId_Id(productId);
        if (rates.isEmpty()) {
            return 0.0;
        }

        double sum = 0.0;
        for (Rate rate : rates) {
            sum += rate.getStar();
        }

        return sum / rates.size();
    }


    @Override
    @Transactional
    public RateResponseDTO createRate(RateDTO rateDTO, MultipartFile[] imageFiles){
        try {
            Rate savedRate = mapRateDTOToEntity(rateDTO);
            savedRate = rateRepository.save(savedRate);

            if (imageFiles != null && imageFiles.length > 0) {
                saveRateImages(imageFiles, savedRate);
            }

            RateResponseDTO rateResponseDTO = new RateResponseDTO();
            rateResponseDTO.setRate(savedRate);
            rateResponseDTO.setLink(getRateImageLinks(savedRate));
            double averageRating = avgByProductId(savedRate.getProductId().getId());
            rateResponseDTO.setAverageRating(averageRating);
            return rateResponseDTO;
        } catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("Failed to create rate");
        }
    }

    private Rate mapRateDTOToEntity(RateDTO rateDTO) {
        System.out.println(rateDTO);
        LocalDateTime currentTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedTime = currentTime.format(formatter);

        Rate rateData = rateDTO.getRate();
        if (rateData == null) {
            throw new IllegalArgumentException("RateDTO object must contain a valid Rate object.");
        }

        Rate rate = new Rate();
        rate.setPhoneNumber(rateData.getPhoneNumber());
        rate.setFullName(rateData.getFullName());
        rate.setEvaluate(rateData.getEvaluate());
        rate.setTime(formattedTime);
        rate.setStar(rateData.getStar());
        rate.setProductId(rateData.getProductId());

        return rate;
    }



    private void saveRateImages(MultipartFile[] imageFiles, Rate savedRate){
        for (MultipartFile imageFile : imageFiles) {
            if(imageFile != null && !imageFile.isEmpty()) {
                try {
                    String imageUrl = firebaseImageService.save(imageFile);
                    createRateImageEntity(savedRate, imageUrl);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    private Set<String> getRateImageLinks (Rate savedRate) {
        Set<String> imageLinks = new HashSet<>();
        List<RateImage> rateImages = rateImageRepository.findAllByRateId_Id(savedRate.getId());
        for (RateImage rateImage : rateImages) {
            imageLinks.add(rateImage.getLink());
        }
        return imageLinks;
    }

    private void createRateImageEntity(Rate savedRate, String imageUrl){
        RateImage rateImage = new RateImage();
        rateImage.setLink(imageUrl);
        rateImage.setRateId(savedRate);
        rateImageRepository.save(rateImage);
    }

}