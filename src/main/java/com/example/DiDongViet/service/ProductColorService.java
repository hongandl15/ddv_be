package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.ProductColorDTO;


import java.util.List;
import java.util.Map;

public interface ProductColorService {
   Map<String, Object> createProductColor(Long productId, List<Long> colorIds);
   ProductColorDTO getListDetailById (Long productId);
   ProductColorDTO updateProductColors(Long productId, List<Long> newColors);

}
