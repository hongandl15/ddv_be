package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forList.CartDTO;
import com.example.DiDongViet.dto.forCreate.CCartDetailDTO;
import com.example.DiDongViet.dto.forDetail.CartDetailDTO;
import com.example.DiDongViet.entity.CartDetail;

import java.util.List;

public interface CartService {
    CartDTO getCartById(Long userId);

    public List<CartDetailDTO> getCartDetailsByCartId(Long userId);

    CartDTO addProductToCart(Long userId, Long productId, Long colorId);

    void clearCart(Long userId);
    void removeCartDetail(Long cartDetailId);
    CartDetailDTO updateCartDetail(Long cartDetailId, CartDetailDTO cartDetailDTO, int quantityChange);
    List<CartDetailDTO> getAllCartDetails(Long userId);
    public CartDTO getCartByUserId(Long userId);
}
