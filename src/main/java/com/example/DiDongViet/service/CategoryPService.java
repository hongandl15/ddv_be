package com.example.DiDongViet.service;

import com.example.DiDongViet.entity.CategoryP;

public interface CategoryPService {
    CategoryP createCategory(CategoryP categoryP);

    CategoryP updateCategoryP(CategoryP categoryP);

    Boolean deleteCategoryP(Long id);


    CategoryP findCategoryPByName(String name);
}
