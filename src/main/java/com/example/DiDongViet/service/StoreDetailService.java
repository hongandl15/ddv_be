package com.example.DiDongViet.service;

import com.example.DiDongViet.entity.StoreDetail;

import java.util.List;
import java.util.Optional;

public interface StoreDetailService {
    StoreDetail createStoreDetail(StoreDetail storeDetail);

    List<StoreDetail> getAllStoresDetail();

    Optional<StoreDetail> getStoreDetailById(Long storeDetailId);

    StoreDetail updateStoreDetail(StoreDetail storeDetail);
    boolean deleteStoreDetailById(Long storeDetailId);

}
