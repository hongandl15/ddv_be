package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forCreate.CRequestVoucherDetail;
import com.example.DiDongViet.entity.VoucherDetail;

public interface VoucherDetailService {
    VoucherDetail insert(CRequestVoucherDetail cRequestVoucherDetail);
}
