package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forList.LProductManufacturer;
import com.example.DiDongViet.entity.Manufacturer;

import java.util.List;

public interface ManufacturerService {
    Manufacturer createManufacturer(Manufacturer manufacturer);

    Manufacturer updateManufacturer(Manufacturer manufacturer);

    Boolean deleteManufacturer(Long id);

    List<Manufacturer> getAllManufacturer();

    List<LProductManufacturer> getProductsByManufacturer(Manufacturer manufacturer);
}
