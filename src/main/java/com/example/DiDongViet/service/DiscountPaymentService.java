package com.example.DiDongViet.service;

import com.example.DiDongViet.dto.forCreate.DiscountPaymentDTO;
import com.example.DiDongViet.dto.forResponse.RDiscountPaymentDTO;
import com.example.DiDongViet.entity.DiscountPayment;

import java.util.List;
import java.util.Optional;

public interface DiscountPaymentService {
    RDiscountPaymentDTO createDiscountPayment(DiscountPaymentDTO discountPayment);
    List<DiscountPayment> getAllDiscountPayment();
    Optional<DiscountPayment> getDiscountPaymentById(Long id);
    RDiscountPaymentDTO updateDiscountPayment(DiscountPaymentDTO discountPaymentDTO);
    boolean deleteDiscountPaymentByTd(Long id);
    List<DiscountPayment> findByDiscountPaymentNameContaining(String name);
}
