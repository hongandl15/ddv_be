package com.example.DiDongViet.service;



import com.example.DiDongViet.dto.*;
import com.example.DiDongViet.entity.*;
import com.example.DiDongViet.security.jwt.JwtAuthResponse;
import org.springframework.security.core.userdetails.UserDetails;


import java.util.List;
import java.util.Optional;

public interface AccountService {
    Optional<Account> getByEmail(String email);
    JwtAuthResponse validateLogin(String email, String password);
    Account create(RegisterModel registerModel);
    void delete(Long id);
    void changeRole(Long id);
    List<Account> getAllAccount();
    List<Account> getByRole(String role);
    void changeStatus(Long id);
    Account changePassword(AccountDTO.ChangePassword changePassword);
    AccountDTO.EmailResult sendResetPassword(AccountDTO.ChangePassword changePassword);
    Account resetPassword(AccountDTO.ChangePassword changePassword);

}
