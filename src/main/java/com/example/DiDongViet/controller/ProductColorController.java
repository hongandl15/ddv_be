package com.example.DiDongViet.controller;

import com.example.DiDongViet.dto.CProductColorDTO;
import com.example.DiDongViet.dto.ProductColorDTO;
import com.example.DiDongViet.dto.UProductColorDTO;
import com.example.DiDongViet.entity.ProductColor;
import com.example.DiDongViet.service.ProductColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/product_color")
public class ProductColorController {
    @Autowired
    private ProductColorService productColorService;
//    @PostMapping("/add/{productId}")
//    public ResponseEntity<Map<String, Object>> createProductColor(@PathVariable Long productId, @RequestBody List<CProductColorDTO> cProductColorDTOList) {
//
//        Map<String, Object> createdProductColor = productColorService.createProductColor(productId, cProductColorDTOList);
//        return ResponseEntity.ok(createdProductColor);
//    }

    @GetMapping("/get-list/{productId}")
    public ResponseEntity<ProductColorDTO> getListDetailById(@PathVariable Long productId) {
        ProductColorDTO productColor = productColorService.getListDetailById(productId);
        return ResponseEntity.ok(productColor);
    }
    @PutMapping("/edit/{productId}")
    public ResponseEntity<ProductColorDTO> updateProductColor(@PathVariable Long productId, @RequestBody UProductColorDTO uProductColorDTO) {
        ProductColorDTO productColorDTO = productColorService.updateProductColors(productId, uProductColorDTO.getColors());
        return ResponseEntity.ok(productColorDTO);
    }



}