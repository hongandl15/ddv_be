package com.example.DiDongViet.controller;

import com.example.DiDongViet.dto.forList.LProductManufacturer;
import com.example.DiDongViet.entity.Manufacturer;
import com.example.DiDongViet.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manufacturer")
public class ManufacturerController {
    private ManufacturerService manufacturerService;
    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }
    @GetMapping("/getAll")
    public ResponseEntity<List<Manufacturer>> getAllManufacturer ()
    {
        List<Manufacturer> manufacturers = manufacturerService.getAllManufacturer();
        return ResponseEntity.ok().body(manufacturers);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Manufacturer> createManufacturer( @RequestBody Manufacturer manufacturer){
        Manufacturer createdManufacturer = manufacturerService.createManufacturer(manufacturer);
        return ResponseEntity.ok().body(createdManufacturer);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<Manufacturer> updateManufacturer( @RequestBody Manufacturer manufacturer){
        Manufacturer updateManufacturer = manufacturerService.updateManufacturer(manufacturer);
        return ResponseEntity.ok().body(updateManufacturer);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deleteManufacturer( @PathVariable(value = "id") Long id){
        boolean deleteManufacturer = manufacturerService.deleteManufacturer(id);
        return ResponseEntity.ok().body(deleteManufacturer);
    }

    @GetMapping("/{manufacturerId}")
    public ResponseEntity<List<LProductManufacturer>> getProductsByManufacturer(@PathVariable Long manufacturerId) {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setId(manufacturerId);
        List<LProductManufacturer> products = manufacturerService.getProductsByManufacturer(manufacturer);
        return ResponseEntity.ok(products);
    }

}
