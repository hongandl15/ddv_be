package com.example.DiDongViet.entity;

import lombok.Data;
import jakarta.persistence.*;

@Entity
@Data
@Table(name = "categoryP")
public class CategoryP {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "categoryP_id")
    private Long id;

    private String categoryPName;
}
