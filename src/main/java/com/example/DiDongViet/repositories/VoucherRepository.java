package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoucherRepository extends JpaRepository<Voucher, Long> {
    List<Voucher> findAllByVoucherNameContains(String s);
    Voucher findByVoucherCode(String code);
//    List<Voucher> findAllByVoucherNameContainsAndVoucherNameContaining(String a, String b);

}
