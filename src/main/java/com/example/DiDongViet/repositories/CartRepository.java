package com.example.DiDongViet.repositories;



import com.example.DiDongViet.entity.Cart;
import com.example.DiDongViet.entity.CartDetail;
import com.example.DiDongViet.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface CartRepository extends JpaRepository<Cart, Long>{
    Optional<Cart> findById(@Param("id")Long cartId);
    Cart findByUserId_Id(Long userId);
}
