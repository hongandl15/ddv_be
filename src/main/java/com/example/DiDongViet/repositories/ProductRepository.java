package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.CategoryC;
import com.example.DiDongViet.entity.Manufacturer;
import com.example.DiDongViet.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository

public interface ProductRepository extends JpaRepository<Product,Long> {
    List<Product> findByCategoryC(CategoryC categoryC);

    List<Product> findByManufacturer(Manufacturer manufacturer);
    List<Product> findByNameContainingIgnoreCase(String keyword);
    Page<Product> findAll(Pageable pageable);



}
