package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Role;
import com.example.DiDongViet.utils.constant.AuthoritiesConstants;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(AuthoritiesConstants name);
}

