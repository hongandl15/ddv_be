package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.DiscountCombo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiscountComboRepository extends JpaRepository<DiscountCombo, Long> {
    List<DiscountCombo> findAllByDiscountNameContains(String s);
}
