package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Product;
import com.example.DiDongViet.entity.ProductColor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProductColorRepository extends JpaRepository<ProductColor, Long> {
    boolean existsByProductAndColorId(Product product, Long colorId);
    List<ProductColor> findAllByProductId(Long productId);
    @Transactional
    List<ProductColor> deleteByProduct(ProductColor productColor);

    @Modifying
    @Query("DELETE FROM ProductColor pi WHERE pi.product.id = :productId")
    @Transactional
    void deleteByProductId(@Param("productId") Long productId);

    ProductColor findByProduct_IdAndColor_Id(Long productId, Long colorId);
}
