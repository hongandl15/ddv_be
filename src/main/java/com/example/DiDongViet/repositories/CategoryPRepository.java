package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.CategoryC;
import com.example.DiDongViet.entity.CategoryP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface CategoryPRepository extends JpaRepository<CategoryP, Long> {
     @Query("SELECT c FROM CategoryP c WHERE c.categoryPName = :name")
     CategoryP findByName(@Param("name") String name);
}