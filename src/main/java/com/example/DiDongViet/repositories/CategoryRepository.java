package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.CategoryC;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryC, Long> {

}
