package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.VoucherDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoucherDetailRepository extends JpaRepository<VoucherDetail, Long> {
}
