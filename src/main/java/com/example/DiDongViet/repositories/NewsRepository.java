package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewsRepository extends JpaRepository<News, Long> {
}
