package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.RateImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RateImageRepository extends JpaRepository<RateImage, Long> {
    List<RateImage> findAllByRateId_Id(Long rateId);

    @Query("SELECT ri FROM RateImage ri WHERE ri.rateId.id = :rateId AND ri.rateId.productId.id = :productId")
    List<RateImage> findAllByRateIdAndProductId(@Param("rateId") Long rateId, @Param("productId") Long productId);

    @Transactional
    void deleteByRateId_Id(Long id);
}
