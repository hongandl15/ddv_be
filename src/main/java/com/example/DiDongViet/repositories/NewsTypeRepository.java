package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.NewsType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewsTypeRepository extends JpaRepository<NewsType, Long> {
}
