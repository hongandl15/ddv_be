package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Cart;
import com.example.DiDongViet.entity.CartDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartDetailRepository extends JpaRepository<CartDetail, Long> {
    List<CartDetail> findByCartId(Cart cartId);

}
