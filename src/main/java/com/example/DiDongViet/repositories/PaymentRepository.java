package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
