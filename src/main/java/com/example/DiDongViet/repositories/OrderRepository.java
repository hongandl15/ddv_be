package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Orders, Long> {
}
