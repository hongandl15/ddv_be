package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Manufacturer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {

}

