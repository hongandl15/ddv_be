package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.DiscountPaymentDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DiscountPaymentDetailRepository extends JpaRepository<DiscountPaymentDetail, Long> {
    /*boolean existsByOrderId(Long orderId);
    Optional<DiscountPaymentDetail> findByOrderId(Long orderId);*/
}
