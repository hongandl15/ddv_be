package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.StoreDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreDetailRepository extends JpaRepository<StoreDetail, Long> {
}
