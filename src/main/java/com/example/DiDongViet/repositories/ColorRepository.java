package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Color;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ColorRepository extends JpaRepository<Color, Long> {
}
