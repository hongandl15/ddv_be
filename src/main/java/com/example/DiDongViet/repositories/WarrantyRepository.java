package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Warranty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WarrantyRepository extends JpaRepository<Warranty, Long> {
}
