package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecificationRepository extends JpaRepository<Specification, Long> {
}
