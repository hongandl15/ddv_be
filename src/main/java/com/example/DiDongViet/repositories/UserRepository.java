package com.example.DiDongViet.repositories;

import com.example.DiDongViet.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public User findByAccount_Id(Long accountId);
    public List<User> findAllBy();
}
