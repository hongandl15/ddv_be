package com.example.DiDongViet.dto.forUpdate;

import com.example.DiDongViet.entity.Product;
import com.example.DiDongViet.entity.Specification;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
public class USpecificationDetailDTO {
    private long id;
    private String nameDetail;
    private String description;

}
