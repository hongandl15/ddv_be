package com.example.DiDongViet.dto.forList;

import com.example.DiDongViet.dto.forDetail.CartDetailDTO;
import com.example.DiDongViet.entity.Cart;
import com.example.DiDongViet.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class CartDTO {
    private Long userId;
    private List<CartDetailDTO> cartDetailDTOList;
}
