package com.example.DiDongViet.dto.forList;

import com.example.DiDongViet.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
@AllArgsConstructor
@Data
@NoArgsConstructor
public class LProductDTO {

    private Long id;

    private String name;

    private Long price;

    private Double rate;

    private String Conditions;

    private String dateWarranty;

    private String imageLinks;
}
