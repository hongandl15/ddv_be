package com.example.DiDongViet.dto.forDetail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartDetailDTO {
    private Long id;
    private String productName;
    private String productImageLink;
    private Integer quantity;
    private Long  price;
    private Long unitPrice;
    private Long productColor;
}
