package com.example.DiDongViet.dto.forDetail;

import com.example.DiDongViet.entity.Product;
import jakarta.persistence.*;
import lombok.*;

import java.util.*;

@Data
public class DProductDTO {
    //private Long id;
    private Product product;
    private List<Map<String, Object>> specificationDetails;
    private Set<String> imageLinks;
}
