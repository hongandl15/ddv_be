package com.example.DiDongViet.dto.forCreate;

import com.example.DiDongViet.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ProductDTO {
    private Product product;
    private MultipartFile[] files;
    private List<Long> colors;

}
