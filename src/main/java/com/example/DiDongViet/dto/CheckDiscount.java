package com.example.DiDongViet.dto;

import com.example.DiDongViet.entity.Orders;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class CheckDiscount {
    private Orders orderId;
}
