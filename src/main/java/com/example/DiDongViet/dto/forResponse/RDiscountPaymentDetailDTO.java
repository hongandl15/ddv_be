package com.example.DiDongViet.dto.forResponse;

import com.example.DiDongViet.entity.DiscountPayment;
import com.example.DiDongViet.entity.Orders;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class RDiscountPaymentDetailDTO {
    private Long id;
    private Orders orderId;
    private DiscountPayment discountPaymentId;
}
