package com.example.DiDongViet;

import com.example.DiDongViet.entity.Account;
import com.example.DiDongViet.entity.Cart;
import com.example.DiDongViet.entity.Role;
import com.example.DiDongViet.entity.User;
import com.example.DiDongViet.repositories.AccountRepository;
import com.example.DiDongViet.repositories.CartRepository;
import com.example.DiDongViet.repositories.RoleRepository;
import com.example.DiDongViet.repositories.UserRepository;
import com.example.DiDongViet.utils.constant.AuthProvider;
import com.example.DiDongViet.utils.constant.AuthoritiesConstants;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springdoc.core.configuration.SpringDocConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@SpringBootApplication
@OpenAPIDefinition
@Import({SpringDocConfiguration.class})
public class Demo6Application {

//    @Autowired
//    private AccountRepository accountRepository;
//    @Autowired
//    private RoleRepository authorityRepository;
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//    @Autowired
//    private UserRepository userRepository;
//    @Autowired
//    private CartRepository cartRepository;
//    @Value("${ADMIN_EMAIL}")
//    private String email;
//    @Value("${ADMIN_PASSWORD}")
//    private String password;

    public static void main(String[] args) {
        SpringApplication.run(Demo6Application.class, args);


    }
//    public void createAdmin() {
//        if (!accountRepository.existsByEmail(email)) {
//
//            Role role = new Role();
//            role.setName(AuthoritiesConstants.ROLE_ADMIN);
//            authorityRepository.save(role);
//
//
//            Optional<Role> authority = authorityRepository.findByName(AuthoritiesConstants.ROLE_ADMIN);
//            Set<Role> authorities = new HashSet<>();
//            Account newAdmin = new Account();
//            newAdmin.setEmail(email);
//            newAdmin.setStatus("ACTIVE");
//            newAdmin.setProvider(AuthProvider.local);
//            newAdmin.setPassword(passwordEncoder.encode(password));
//            authority.ifPresent(auth -> authorities.add(auth));
//            newAdmin.setRoles(authorities);
//            Account resultAccount = accountRepository.save(newAdmin);
//
//
//            User user = new User();
//            user.setFullName("ADMIN");
//            user.setPhoneNumber("0523467814");
//            user.setDateOfBirth("12/7/2023");
//            user.setAvatar("https://firebasestorage.googleapis.com/v0/b/didongviet-5f99c.appspot.com/o/z4506645735041_37c7c30d5a45340fbf62c96e5988caa6.jpg?alt=media&token=14c30eda-0c99-444b-ab87-c9997abe3775");
//            user.setCreateAt(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
//            user.setAccount(resultAccount);
//            User resultUser = userRepository.save(user);
//
//            Cart newCart = new Cart();
//            newCart.setUserId(resultUser);
//            cartRepository.save(newCart);
//        }
//    }

}
