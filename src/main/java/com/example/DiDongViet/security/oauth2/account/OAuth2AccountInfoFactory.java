package com.example.DiDongViet.security.oauth2.account;

import com.example.DiDongViet.utils.constant.AuthProvider;
import com.example.DiDongViet.utils.exception.ApiException;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class OAuth2AccountInfoFactory {
    public static OAuth2AccountInfo getOAuth2AccountInfo(String registrationId, Map<String, Object> attributes) {
        if(registrationId.equalsIgnoreCase(AuthProvider.google.toString())) {
            return new GoogleOAuth2AccountInfo(attributes);
        } else {
            throw new ApiException("Sorry! Login with " + registrationId + " is not supported yet.", HttpStatus.BAD_REQUEST);
        }
    }
}
