package com.example.DiDongViet.security.jwt;

import com.example.DiDongViet.dto.AccountDTO;
import com.example.DiDongViet.entity.Account;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JwtAuthResponse implements Serializable {
    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;
    private AccountDTO.Response account;
    private String type = "Bearer";

    public JwtAuthResponse(String token, AccountDTO.Response account) {
        this.token = token;
        this.account = account;
    }


    public String getToken() {
        return this.token;
    }

    public AccountDTO.Response getAccount() {
        return this.account;
    }

}
